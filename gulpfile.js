'use strict'; // Помогает увидеть где ошибка.

var gulp = require('gulp'), // Вызов функции gulp по умолчанию.
concatCss = require('gulp-concat-css'), // Склейка файлов. https://www.npmjs.com/package/gulp-concat-css
autoprefixer = require('gulp-autoprefixer'), // Авто префиксер. https://www.npmjs.com/package/gulp-autoprefixer
livereload = require('gulp-livereload'), // https://www.npmjs.com/package/gulp-livereload
jade = require('gulp-jade'), // Плагин для Jade
stylus = require('gulp-stylus'), // Плагин для Stylus
browserSync = require('browser-sync'),
newer = require('gulp-newer'),
plumber = require('gulp-plumber'),// Проверка на ощибки
reload = browserSync.reload;

//All browserSync
gulp.task('server', function() {
  browserSync({
    server: {
      baseDir: 'public'
    }
  });
});


// Complite STYLUS and automatically Prefix CSS
gulp.task('stylus', function() {
  return gulp.src(['./assets/app/**/*.styl', '!./assets/app/**/_*.styl'])
            .pipe(stylus())
            .pipe(plumber())
        .on('error', console.error.bind(console))
        .pipe(autoprefixer({
                browsers: ['last 3 versions'],
                cascade: false
            }))
        .pipe(gulp.dest('./public/app'))
        .pipe(browserSync.reload({stream: true}));
    });

//Complite html
gulp.task('jade', function() {
  return gulp.src(['./assets/**/*.jade', '!./assets/**/_*.jade', '!./assets/pages/blocks.jade'])
            .pipe(jade({
              pretty: true,
              basedir: 'assets'
            }))
          .pipe(plumber())
          .on('error', console.error.bind(console))
          .pipe(gulp.dest('./public/'))
          .pipe(browserSync.reload({stream: true}));
          });

//Blocks
gulp.task('blocks', ['jade'], function() {
  return gulp.src('./assets/pages/blocks.jade')
            .pipe(jade({
              pretty: true,
              basedir: 'assets'
            }))
          .pipe(plumber())
          .on('error', console.error.bind(console))
          .pipe(gulp.dest('./public/pages/'))
          .pipe(browserSync.reload({stream: true}));
          });

// Copy All Files At (images)
gulp.task('images', function() {
  return gulp.src(['./assets/img/**', '!assets/img/svg', '!assets/img/png'])
          .pipe(newer('./public/img'))
          .pipe(gulp.dest('./public/img'))
          .pipe(browserSync.reload({stream: true}));
      });

gulp.task('_images', function() {
  return gulp.src('./assets/_img/**')
          .pipe(newer('./public/_img'))
          .pipe(gulp.dest('./public/_img'))
          .pipe(browserSync.reload({stream: true}));
      });

// Copy All Files At (app)
gulp.task('app', function() {
  return gulp.src(['./assets/app/vendor/*.*', './assets/app/vendor/**/*.*'])
          .pipe(newer('./public/app/vendor'))
          .pipe(gulp.dest('./public/app/vendor'))
          .pipe(browserSync.reload({stream: true}));
      });

// Copy Web Fonts To Dist
gulp.task('font', function() {
  return gulp.src('./assets/font/**')
          .pipe(newer('./public/font'))
          .pipe(gulp.dest('./public/font'))
          .pipe(browserSync.reload({stream: true}));
      });

// Watch everything
gulp.task('watch', function() {
  gulp.watch('./assets/blocks/**/*.styl',['stylus']);
  gulp.watch('./assets/blocks/**/*.jade', ['jade']);
  gulp.watch('./assets/app/vendor/**/*.*', ['app']);
  gulp.watch('./assets/img/*', ['images']);
  gulp.watch('./assets/_img/*', ['_images']);
  gulp.watch('./assets/font/*', ['font']);
  gulp.watch('./assets/app/_*.styl',['stylus']);
  gulp.watch('./assets/pages/*',['blocks']);
});


gulp.task('default', ['jade', 'server', 'images', 'font', 'app', 'stylus', 'watch', '_images', 'blocks']);